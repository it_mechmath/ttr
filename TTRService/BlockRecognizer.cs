﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Patagames.Ocr;
using Patagames.Ocr.Enums;
using System.Drawing;

namespace TTRService
{
    public class BlockRecognizer
    {
        Bitmap image;
        public BlockRecognizer(Image img)
        {
            image = new Bitmap(img);
        }

        public string ToText()
        {
            using (var api = OcrApi.Create())
            {
                api.Init(Languages.English);
                string plainText = api.GetTextFromImage(image);
                return plainText;
            }
        }
    }
}