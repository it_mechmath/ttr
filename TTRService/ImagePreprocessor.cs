﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace TTRService
{
    public class ImagePreprocessor
    {
        Bitmap image;

        public ImagePreprocessor(Image img)
        {
            image = new Bitmap(img);
        }

        public Image Process()
        {
            //grayscale
            image = GrayScale(image);

            // filters ..

            return image;
        }

        private Bitmap GrayScale(Bitmap Bmp)
        {
            int rgb;
            Color c;

            for (int y = 0; y < Bmp.Height; y++)
                for (int x = 0; x < Bmp.Width; x++)
                {
                    c = Bmp.GetPixel(x, y);
                    rgb = (int)((c.R + c.G + c.B) / 3);
                    Bmp.SetPixel(x, y, Color.FromArgb(rgb, rgb, rgb));
                }
            return Bmp;
        }
    }
}