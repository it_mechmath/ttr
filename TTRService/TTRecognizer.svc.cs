﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace TTRService
{
    public class TTRecognizer : ITTRecognizer
    {
        public CsvResponse Convert(ImgRequest shotdata)
        {
            ImagePreprocessor ip = new ImagePreprocessor(shotdata.image);
            shotdata.image = ip.Process(); //Подготавливаем снимок

            // Заглушка, тут будет второй модуль - блочное распознавание.
            BlockRecognizer br = new BlockRecognizer(shotdata.image);
            string text = br.ToText();

            return new CsvResponse { Accuracy = .0, csvFileBytes = Encoding.UTF8.GetBytes(text) };
        }
    }
}
