﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace TTRService
{
    [ServiceContract]
    public interface ITTRecognizer
    {
        /// <summary>
        /// Главный и единственный эндпоинт в проекте. Принимает изображение расписания и выдает CSV 
        /// с параметром точности распознавания всего расписания.
        /// </summary>
        [OperationContract]
        [WebInvoke(UriTemplate = "/convert", Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        CsvResponse Convert(ImgRequest shotdata);
    }


    /// <summary>
    /// Контракт ответа главного метода. Точность и сформированная таблица.
    /// </summary>
    [DataContract]
    public class CsvResponse
    {
        [DataMember]
        public double Accuracy { get; set; }

        [DataMember]
        public string csvFileBase64 { get; set; }

        [IgnoreDataMember]
        public byte[] csvFileBytes { get; set; }

        [OnSerialized]
        void OnSerialized(StreamingContext context)
        {
            if (csvFileBytes.Any())
            {
                csvFileBase64 = Convert.ToBase64String(csvFileBytes);
            }
        }
    }

    /// <summary>
    /// Контракт запроса главного метода. Тип таблицы и её изображение.
    /// </summary>
    [DataContract]
    public class ImgRequest
    {
        [DataMember]
        public TableType tableType { get; set; }

        [DataMember]
        public string imageBase64 { get; set; }

        [IgnoreDataMember]
        public Image image { get; set; }

        [OnDeserialized]
        void OnDeserialized(StreamingContext context)
        {
            if (!string.IsNullOrEmpty(imageBase64))
            {
                byte[] imageBytes = Convert.FromBase64String(imageBase64);

                using (var ms = new MemoryStream(imageBytes))
                {
                    image = Image.FromStream(ms);
                }
            }
        }
    }

    [DataContract]
    public enum TableType
    {
        [EnumMember]
        StudentTable,

        [EnumMember]
        Unknown
    }
}
